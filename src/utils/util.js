export function getValueForm(event, state) {
    const { properties } = state;
    const update = {
        ...properties, [event.target.id]: event.target.value
    };
    return update;
}
