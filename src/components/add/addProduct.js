import { Button, Grid, TextField } from "@material-ui/core";
import axios from "axios";
import React from "react";


export class AddProductComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            product: {
                name: '',
                price: '',
                currency: '',
                sku: ''
            }
        };

        this.changeValue = this.changeValue.bind(this);
        this.submit = this.submit.bind(this);
    }

    submit(event) {
        event.preventDefault();
        axios.post(`http://localhost:4000/addProduct`, this.state.product).then(
            (res) => {
                this.props.history.goBack();
            }
        );

    }

    changeValue(event) {
        const { product } = this.state;
        const updateProduct = {
            ...product, [event.target.id]: event.target.value
        };
        this.setState({ product: updateProduct });
    }

    render() {
        return (
            <form onSubmit={this.submit}>
                <Grid container spacing={3}
                    direction="column"
                    justifyContent="center"
                    alignItems="center">

                    <Grid item>
                        <TextField id="sku" variant="outlined" value={this.state.product.sku}
                            onChange={this.changeValue} placeholder="Sku" />
                    </Grid>
                    <Grid item>
                        <TextField id="name" variant="outlined" value={this.state.product.name}
                            onChange={this.changeValue} placeholder="Nombre Producto" />
                    </Grid>
                    <Grid item>
                        <TextField id="price" variant="outlined" value={this.state.product.price}
                            onChange={this.changeValue} placeholder="Valor" />
                    </Grid>
                    <Grid item>
                        <TextField id="currency" variant="outlined" value={this.state.product.currency}
                            onChange={this.changeValue} placeholder="Tipo Divisa" />
                    </Grid>

                    <Grid>
                        <Button variant="contained" type="submit" color="primary">
                            Agregar
                        </Button>
                    </Grid>
                </Grid>

            </form>
        );
    }

}