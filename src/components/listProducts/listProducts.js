import React from 'react';
import axios from 'axios'
import { Button, Card, CardActions, CardContent, Grid, Typography } from '@material-ui/core';

import {
    Link
} from "react-router-dom";
export class ListProductComponent extends React.Component {

    state = {
        products: []
    }

    componentDidMount() {
        this.refresh();
    }

    refresh() {
        axios.get('http://localhost:4000/getProducts').then(
            (res) => {
                const products = res.data;
                this.setState({ products });
            }
        );
    }

    removeItem(sku) {
        axios.delete(`http://localhost:4000/removeProduct/${sku}`).then(
            (res) => {
                this.refresh();
            }
        );

    }

    render() {

        return (
            <Grid container spacing={2} justifyContent="center">
                {
                    this.state.products.map((product, i) =>
                        <Grid item xs={3} key={i}>
                            <Card>
                                <CardContent>
                                    <Typography color="textSecondary" gutterBottom>
                                        {product.name}
                                    </Typography>
                                    <Typography variant="h5" component="h2">
                                        {product.price}
                                    </Typography>
                                    <Typography variant="body2" component="p">
                                        {product.currency}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" variant="contained" onClick={this.removeItem.bind(this, product.sku)}>
                                        Remove
                                    </Button>

                                    <Link to={{ pathname: '/update', state: product }}>
                                        Actualizar
                                    </Link>

                                </CardActions>
                            </Card>
                        </Grid>
                    )
                }
            </Grid >
        );
    }
}


