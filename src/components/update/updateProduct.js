import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import React from 'react';

export class UpdateProductComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = { product: props.location.state }

        this.changeValue = this.changeValue.bind(this);
        this.submit = this.submit.bind(this);
    }

    submit(event) {
        event.preventDefault();
        axios.put(`http://localhost:4000/updateProduct`, this.state.product).then(
            (res) => {
                this.props.history.goBack();
            }
        );

    }

    changeValue(event) {
        const { product } = this.state;
        const updateProduct = {
            ...product, [event.target.id]: event.target.value
        };
        this.setState({ product: updateProduct });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.submit}>
                    <Grid container spacing={3}
                        direction="column"
                        justifyContent="center"
                        alignItems="center">

                        <Grid item>
                            <TextField id="name" variant="outlined" defaultValue={this.state.product.name} onChange={this.changeValue} />
                        </Grid>
                        <Grid item>
                            <TextField id="price" variant="outlined" defaultValue={this.state.product.price} onChange={this.changeValue} />
                        </Grid>
                        <Grid item>
                            <TextField id="currency" variant="outlined" defaultValue={this.state.product.currency} onChange={this.changeValue} />
                        </Grid>

                        <Grid>
                            <Button variant="contained" type="submit">
                                Actualizar
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </div>
        );
    }
}


