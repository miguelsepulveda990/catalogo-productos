import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import './header.scss';
import {
    Link
} from "react-router-dom";
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        paddingBottom: 10
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export function HeaderComponent() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Catalogo
                    </Typography>
                    <Link to="/list" className="nounderline">
                        <Button variant="contained" color="secondary">
                            Lista Producto
                        </Button>
                    </Link>
                    <Link to="/add" className="nounderline green">
                        <Button variant="contained" color="secondary" className="green">
                            Agregar Producto
                        </Button>
                    </Link>
                </Toolbar>
            </AppBar>
        </div>
    );
}
