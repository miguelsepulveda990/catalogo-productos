import { Grid } from '@material-ui/core';
import './App.css';
import { HeaderComponent } from './components/header/header';
import { ListProductComponent } from './components/listProducts/listProducts';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { UpdateProductComponent } from './components/update/updateProduct';
import { AddProductComponent } from './components/add/addProduct';

function App() {
  return (
    <Router className="App">
      <Grid container direction="column">
        <HeaderComponent></HeaderComponent>
      </Grid>

      <Switch>
        <Route path="/update" component={UpdateProductComponent} />
        <Route path="/list" component={ListProductComponent} />
        <Route path="/add" component={AddProductComponent} />
      </Switch>
    </Router >
  );
}

export default App;
